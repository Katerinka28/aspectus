module.exports = {
  title: 'ASPECTUS',
  description: 'Set of tools for application development',
  base: '/aspectus/',
  plugins: [
    '@vuepress/back-to-top',
    ['@vuepress/pwa', { serviceWorker: true, updatePopup: false }],
    'demo-block'
  ],
  chainWebpack(config) {
    config.resolve.alias.set('vue$', 'vue/dist/vue.common');
    config.node.set('global', true);
  },
  head: [
    ['meta', { charset: 'utf-8' }],
    ['meta', { name: 'viewport', content: 'width=device-width, initial-scale=1' }],
    ['meta', { property: 'og:image', content: 'https://preusx.gitlab.io/aspectus/logo.png' }],
    ['meta', { name: 'msapplication-TileColor', content: '#ffffff' }],
    ['meta', { name: 'msapplication-TileImage', content: '/img/favicon/ms-icon-144x144.png' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'white' }],
    ['meta', { name: 'apple-mobile-web-app-title', content: 'Aspectus' }],
    ['meta', { name: 'mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'application-name', content: 'Aspectus' }],
    ['meta', { name: 'theme-color', content: '#7d4cdb' }],
    ['link', { rel: 'apple-touch-icon', sizes: '57x57', href: '/img/favicon/apple-icon-57x57.png' }],
    ['link', { rel: 'apple-touch-icon', sizes: '60x60', href: '/img/favicon/apple-icon-60x60.png' }],
    ['link', { rel: 'apple-touch-icon', sizes: '72x72', href: '/img/favicon/apple-icon-72x72.png' }],
    ['link', { rel: 'apple-touch-icon', sizes: '76x76', href: '/img/favicon/apple-icon-76x76.png' }],
    ['link', { rel: 'apple-touch-icon', sizes: '114x114', href: '/img/favicon/apple-icon-114x114.png' }],
    ['link', { rel: 'apple-touch-icon', sizes: '120x120', href: '/img/favicon/apple-icon-120x120.png' }],
    ['link', { rel: 'apple-touch-icon', sizes: '144x144', href: '/img/favicon/apple-icon-144x144.png' }],
    ['link', { rel: 'apple-touch-icon', sizes: '152x152', href: '/img/favicon/apple-icon-152x152.png' }],
    ['link', { rel: 'apple-touch-icon', sizes: '180x180', href: '/img/favicon/apple-icon-180x180.png' }],
    ['link', { rel: 'icon', type: 'image/png', sizes: '192x192',  href: '/img/favicon/android-icon-192x192.png' }],
    ['link', { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/img/favicon/favicon-32x32.png' }],
    ['link', { rel: 'icon', type: 'image/png', sizes: '96x96', href: '/img/favicon/favicon-96x96.png' }],
    ['link', { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/img/favicon/favicon-16x16.png' }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],

    ['script', { src: 'https://cdn.jsdelivr.net/npm/@babel/standalone/babel.min.js' }],
  ],
  locales: {
    '/': {
      lang: 'en-US'
    },
    '/ru/': {
      lang: 'ru-RU',
      title: 'ASPECTUS',
      description: 'Набор утилит для разработки приложения',
    }
  },
  themeConfig: {
    repo: 'https://gitlab.com/preusx/aspectus',
    docsRepo: 'https://gitlab.com/preusx/aspectus',
    docsDir: 'docs',
    logo: '/logo-alter.svg',
    editLinks: true,
    sidebarDepth: 2,
    locales: {
      '/ru/': {
        label: 'Русский',
        selectText: 'Языки',
        editLinkText: 'Помогите нам исправить эту страницу!',
        lastUpdated: 'Изменено',
        nav: [
          { text: 'Пакеты', link: '/packages/bem.html' },
          // { text: 'Модули', link: '/ru/modules/base/' }
        ],
      },
      '/': {
        label: 'English',
        selectText: 'Languages',
        editLinkText: 'Help us improve this page!',
        lastUpdated: 'Last Updated',
        nav: [
          { text: 'Packages', link: '/packages/bem.html' },
          // { text: 'Modules', link: '/ru/modules/base/' }
        ],
        sidebar: {
          '/packages/': [
            {
              title: 'BEM',
              type: 'group',
              collapsable: true,
              children: [
                'bem',
                'vue-bem',
                'vue-bem-styling',
              ]
            },
            {
              title: 'Element queries',
              type: 'group',
              collapsable: true,
              children: [
                'eq',
                'vue-eq',
              ]
            },
            {
              title: 'Dermis',
              type: 'group',
              collapsable: true,
              children: [
                'vue-grid',
                'vue-dermis',
              ]
            },
            {
              title: 'Promise',
              type: 'group',
              collapsable: true,
              children: [
                'promise-proxy',
                'cancellable-promise-proxy',
                'progress-promise-proxy',
                'cancellable-promise',
              ]
            },
            {
              title: 'Resource',
              type: 'group',
              collapsable: true,
              children: [
                'resource',
                'resource-template-url-getter',
                'cancellable-fetch',
                'superagent-fetch',
              ]
            },
            {
              title: 'Permissions',
              type: 'group',
              collapsable: true,
              children: [
                'permissions',
                'vue-permissions',
                'vue-router-permissions',
              ]
            },
            {
              title: 'Forms',
              type: 'group',
              collapsable: true,
              children: [
                {
                  title: 'Controls',
                  type: 'group',
                  collapsable: true,
                  children: [
                    'vue-control-label',
                    'vue-control-hint',
                    'vue-control-descriptor',
                    'vue-control-interaction-state-controller',
                  ]
                },
                {
                  title: 'Validation',
                  type: 'group',
                  collapsable: true,
                  children: [
                    'vee-optional-provider',
                    'vee-control-descriptor',
                    'vee-i18n'
                  ]
                },
              ]
            },
            {
              title: 'Vue Router',
              type: 'group',
              collapsable: true,
              children: [
                'vue-route-installer',
                'vue-router-permissions',
                'vue-router-parameters-controller',
              ]
            },
            {
              title: 'DOM',
              type: 'group',
              collapsable: true,
              children: [
                'dom-mount',
              ]
            },
            {
              title: 'Selection',
              type: 'group',
              collapsable: true,
              children: [
                'selection-controller',
                'vue-selection-controller',
                'vue-all-selection-controller',
              ]
            },
            {
              title: 'Uploading',
              type: 'group',
              collapsable: true,
              children: [
                'vue-dropzone',
                'vue-dropzone-uploader',
              ]
            },
            {
              title: 'Utilities',
              type: 'group',
              collapsable: true,
              children: [
                'vue-loading-state-mixin',
                'vue-tag',
                'vue-trans',
                'vue-render-slot',
                'vue-utils',
              ]
            },
          ],
        },
      }
    }
  },
  postcss: {
    plugins: [
      require('autoprefixer')({}),
      require('css-mqpacker')({
        sort: true,
      }),
    ]
  }
};
