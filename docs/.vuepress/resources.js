import { partial, compose } from 'ramda';
import EventEmitter from '@aspectus/event-emitter';
import { baseResource } from '@aspectus/resource';
import cancellablePromiseProxy from '@aspectus/cancellable-promise-proxy';
import progressPromiseProxy from '@aspectus/progress-promise-proxy';

export function makeRequest(proxy, url, {
  timeout = 2000,
  tick = 10,
  response = {},
} = {}) {
  const emitter = new EventEmitter();
  let loaded = 0;
  const getResponse = () => typeof response === 'function' ? response() : response;

  return proxy(
    new Promise((resolve, reject) => {
      let tickTimeout = null;
      const ticker = () => {
        loaded += tick;
        emitter.emit('progress', { loaded, total: timeout })
        setTimeout(ticker, tick);
      };

      const generalTimeout = setTimeout(() => {
        resolve(getResponse());
        clearTimeout(tickTimeout);
      }, timeout);

      emitter.on('abort', () => {
        reject(getResponse());
        clearTimeout(generalTimeout);
      });
      ticker();
    }),
    {
      cancelController: { abort: () => emitter.emit('abort') },
      progressEmitter: emitter,
    }
  );
}

export const baseProxy = compose(cancellablePromiseProxy, progressPromiseProxy);
export const fetcher = partial(makeRequest, [baseProxy]);
export const resource = baseResource.url(() => null).fetcher(fetcher);
