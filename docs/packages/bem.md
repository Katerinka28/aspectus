# Utilities for `bem` classes creation

Set of function utilities to make bem classes generation easier.

## Installation

```
yarn add @aspectus/bem
```

## Usage

```js
import { createStateGenerator, createBlockNameGenerator } from '@aspectus/bem';

const blockName = createBlockNameGenerator();

console.log(blockName('block', { modifier: 'value', truthful: true }));
// > ['block', 'block--truthful']
console.log(blockName('block', 'element', { modifier: 'value' }));
// > ['block__element', 'block__element--modifier_value']

const state = createStateGenerator();

console.log(state({ state: 'value', other: true }));
// > ['is-state_value', 'is-other']
```

You may configure delimiters used to generate classnames:

Block:
- `n` - Namespace. `''` by default.
- `e` - Element delimiter. `'__'` by default.
- `m` - Modifier delimiter. `'--'` by default.
- `v` - Modifier value delimiter. `'_'` by default.

State:
- `sp` - State prefix. `'is-'` by default.
- `v` - State value delimiter. `'_'` by default.

Example:

```js
import { createStateGenerator, createBlockNameGenerator } from '@aspectus/bem';

const blockName = createBlockNameGenerator({ n: 'o-', e: '-', m: '_', v: '--' });

console.log(blockName('block', { modifier: 'value', truthful: true }));
// > ['o-block', 'o-block_truthful']
console.log(blockName('block', 'element', { modifier: 'value' }));
// > ['o-block-element', 'o-block-element_modifier--value']

const state = createStateGenerator({ sp: 'st-', v: '--' });

console.log(state({ state: 'value', other: true }));
// > ['st-state--value', 'st-other']
```
