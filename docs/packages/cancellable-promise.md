# Cancellable Promise API [DEPRECATED]

::: danger DEPRECATED
This package will not receive updates anymore.

Use `@aspectus/cancellable-promise-proxy` instead. It's easier to compose.
:::

Simple Promise extension to use `AbortController` to cancel underlying process.

## Installation

```
yarn add @aspectus/cancellable-promise
```

## Usage

For example to make cancellable fetch:

```js
import CancellablePromise from '@aspectus/cancellable-promise';

const controller = new AbortController();
const promise = new CancellablePromise((resolve, reject) => {
  fetch('http://google.com', { signal: controller.signal })
    .then(resolve, reject);
}, controller);

promise.cancel();
```

Method `.cancel` is available after any amount of `.then()` and `.catch()` calls. It will call controller's `.abort` method.
