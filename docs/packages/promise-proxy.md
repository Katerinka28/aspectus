# Promise proxy

Composable promise behavior extension.

## Installation

```
yarn add @aspectus/promise-proxy
```

## Usage

```js
import { createProxy, composeProxies } from '@aspectus/promise-proxy';

// Proxy to add alias for `.then` method.
const chainAliasProxy = {
  chain() {
    return this.then.apply(this, arguments);
  },
};

// Proxy to add a simple method to the promise, that returns passed data.
// All the data is stored in `$data` property.
const returnProxy = createProxy({
  returnSome() {
    return this.$data.some;
  },
});

const proxy = composeProxies(chainAliasProxy, returnProxy);

proxy(Promise.resolve(20))
  .then(x => x)
  .chain(console.log);
// > 20

// First argument is promise or proxy object, the second one is provided data
const promise = proxy(Promise.resolve(20), { some: 40 })
  .then(x => { 'do some stuff' })
  .catch(e => { 'catch an error' })

console.log(promise.returnSome());
// > 40
```

Added methods will available after any amount of `.then()`, `.finally()` and `.catch()` calls.

Promises, that created using `createProxy` method might be executed by their own, without composing them.

Ramda's or other's `compose`/`pipe` method will work only with proxies created with `createProxy` method.
