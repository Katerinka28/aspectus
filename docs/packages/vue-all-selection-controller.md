# Vue AllSelectionController component

Component to handle multiple elements selection with `select all` action. Similar to gmail's multiple email selection behavior with ability to select those that are not on this page and exclude any from that selection.

## Installation

```
yarn add @aspectus/vue-all-selection-controller
```

```js
import VueAllSelectionController from '@aspectus/vue-all-selection-controller';

Vue.use(VueAllSelectionController);
// Or
Vue.use(VueAllSelectionController, {
  name: 'v-all-selection-controller',
});
```

## Usage

Props:

- `items`: Items on this page.
- `amount`: Total amount of items available for selection
- `key-getter`: Key getter function for `@aspectus/selection-controller` usage.

Slot:

- `keyGetter`: Same as appropriate prop.

::: demo
```html
<template>
  <div class="t-content">
    <vue-all-selection-controller
      :items="items"
      :keyGetter="$options.keyGetter"
      :amount="pagination.total"
      v-slot="selection"
    >
      <table>
        <thead>
          <tr>
            <th width="1">
              <input
                type="checkbox"
                :checked="selection.isAll || selection.isTotal"
                :indeterminate.prop="!selection.isAll && selection.isSelected"
                @click="selection.changeSelected(
                  (selection.isAll || selection.isTotal) ? [] : items
                )"
              />
            </th>
            <th>
              Display

              <span v-if="selection.isAll || selection.isTotal">
                <a
                  href=""
                  @click.prevent="selection.changeTotal(!selection.isTotal)"
                >
                  {{ selection.isTotal ? 'Unselect all' : 'Select all' }}
                </a>

                {{ `Total selected: ${selection.selectedAmount}` }}
              </span>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="item in items" :key="$options.keyGetter(item)">
            <td>
              <input
                type="checkbox"
                :checked="selection.checkSelected(item)"
                @click="selection.changeSelected(item)"
              />
            </td>
            <td>{{ item.display }}</td>
          </tr>
        </tbody>
      </table>
    </vue-all-selection-controller>
    <div>
      Page:
      <a
        href=""
        @click.prevent="pagination.page = page"
        v-for="page in pagination.pages"
        :key="page"
        style="margin-right: 1rem"
      >{{ page }}</a>
    </div>
  </div>
</template>
<script>
export default {
  keyGetter: x => x.id,

  data() {
    return {
      pages: [
        [
          { id: 1, display: 'First one' },
          { id: 2, display: 'Second one' },
          { id: 3, display: 'Third one' },
        ],
        [
          { id: 4, display: 'Fourth one' },
          { id: 5, display: 'Fifth one' },
          { id: 6, display: 'Sixth one' },
        ],
        [
          { id: 7, display: 'Seventh one' },
          { id: 8, display: 'Eight one' },
          { id: 9, display: 'Nine one' },
        ],
      ],
      pagination: {
        total: 9,
        page: 1,
        pages: [1, 2, 3],
      },
    };
  },

  computed: {
    items() {
      return this.pages[this.pagination.page - 1];
    }
  }
};
</script>
```
:::
