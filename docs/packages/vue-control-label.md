# Vue control label

Simple functional component for `control-label` element.

## Installation

```sh
yarn add @aspectus/vue-control-label
```

## Usage

```js
import LabelPlugin from '@aspectus/vue-control-label';

Vue.use(LabelPlugin);
// Or
Vue.use(LabelPlugin, { name: 'c-label' });
```

::: demo
```html
<template>
  <div>
    <control-label>Label</control-label>
    <control-label variant="default">Label with variant</control-label>
    <control-label variant="simple" required disabled kind="singular" theme="dark">Label with states and modifiers</control-label>
  </div>
</template>
<script> export default {}; </script>
```
:::
