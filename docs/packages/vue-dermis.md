# Vue `dermis` components

Dermis blocks turned into vue components.

## Installation

```
yarn add @aspectus/vue-dermis
```

Package is dependent on `vue-tag` cause all components are using it internally.

```js
import TagPlugin from '@aspectus/vue-tag';
import DermisPlugin from '@aspectus/vue-dermis';

Vue.use(TagPlugin);

Vue.use(DermisPlugin);
// Or
Vue.use(DermisPlugin, {
  prefix: 'ds-',
  prefixType: 't-',
  prefixUtils: 'u-',
});
```

## Examples

### Caption

::: demo
```html
<template>
  <div>
    <ds-caption size="6" :variant="['uppercase', 'strong']">Label</ds-caption>
    <ds-caption size="2">Caption</ds-caption>
  </div>
</template>
<script> export default {}; </script>
```
:::

### Inliner

::: demo
```html
<template>
  <div>
    <ds-inliner>
      <ds-caption size="6" variant="strong">L</ds-caption>
    </ds-inliner>
    <ds-inliner>
      <ds-caption size="2">C</ds-caption>
    </ds-inliner>
  </div>
</template>
<script> export default {}; </script>
```
:::

### Link

::: demo
```html
<template>
  <div>
    <ds-link styling="default" tag="a">Some link</ds-link>
  </div>
</template>
<script> export default {}; </script>
```
:::

### Avatar

::: demo
```html
<template>
  <div>
    <row>
      <cell cols="narrow">
        <ds-avatar size="lg" src="https://picsum.photos/100" />
      </cell>
      <cell cols="narrow">
        <ds-avatar size="lg" label="Some" />
      </cell>
      <cell cols="narrow">
        <ds-avatar appearance="rounded" size="lg" label="Rounded" />
      </cell>
    </row>
  </div>
</template>
<script> export default {}; </script>
```
:::

### ...And so on
