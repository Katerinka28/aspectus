# Vue Dropzone uploader component

File uploader component for vue. Based on `@aspectus/vue-dropzone` component.

## Installation

```
yarn add @aspectus/vue-dropzone-uploader
```

```js
import VueDropzoneUploader from '@aspectus/vue-dropzone-uploader';

Vue.use(VueDropzoneUploader);
// Or
Vue.use(VueDropzoneUploader, {
  name: 'v-dropzone-uploader',
});
```

## Usage

::: warning No styling!
This package does not provide any styling, so you should add the base styling by your own.
:::

::: demo
```html
<template>
  <vue-dropzone-uploader
    style="position: relative"
    :resource="$testResources.uploadResource"
    @upload="uploadHandler"
    ref="uploader"
  >
    <template #cover>
      Можно отпускать.
    </template>

    <template #default="uploader">
      <div @click="open" style="padding: 1rem;">
        <div style="margin-bottom: 1rem;">
          Перенесите файлы сюда, или кликните для загрузки.
        </div>
        <row appearance="spaced">
          <cell
            cols="12 6-sm 4-md"
            v-for="file in value"
            :key="file.id"
            @click.stop=""
          >
            <div>{{ file.name }}</div>
            <div>
              <ds-link
                @click.prevent="remove(file)" styling="default"
              >
                Remove
              </ds-link>
            </div>
          </cell>

          <cell
            cols="12 6-sm 4-md"
            v-for="file in uploader.accepted"
            :key="file.id"
            @click.stop=""
          >
            <div>{{ file.file.name }}</div>
            <div>
              <ds-link
                @click.prevent="uploader.remove(file)" styling="default"
              >
                Remove
              </ds-link>
              <ds-link
                @click.prevent="uploader.upload(file)" styling="default"
              >
                Start upload
              </ds-link>
            </div>
            <div>
              <progress max="1" :value="file.progress.relative" style="height: 0.25rem"/>
            </div>
          </cell>
        </row>
      </div>
    </template>
  </vue-dropzone-uploader>
</template>
<script>
export default {
  data() {
    return {
      value: [],
    };
  },
  methods: {
    uploadHandler({ result, descriptor }) {
      this.$refs.uploader.remove(descriptor);

      this.input(this.value.concat([result]));
    },
    input(data) {
      this.value = data;
    },
    open() {
      this.$refs.uploader.open();
    },
    remove(file) {
      const { id } = file;

      const index = this.value.findIndex(x => x.id === id);
      if (index !== -1) {
        this.input(this.value.slice(0, index).concat(this.value.slice(index + 1)));
      }
    },
  },
};
</script>
<style>
.vue-dropzone__input { display: none; }
</style>
```
:::

### Autoupload

::: demo
```html
<template>
  <vue-dropzone-uploader
    style="position: relative"
    :resource="$testResources.uploadResource"
    @upload="uploadHandler"
    ref="uploader"
    autoload
  >
    <template #cover>
      Можно отпускать.
    </template>

    <template #default="uploader">
      <div @click="open" style="padding: 1rem;">
        <div style="margin-bottom: 1rem;">
          Перенесите файлы сюда, или кликните для загрузки.
        </div>
        <row appearance="spaced">
          <cell
            cols="12 6-sm 4-md"
            v-for="file in value"
            :key="file.id"
            @click.stop=""
          >
            <div>{{ file.name }}</div>
            <div>
              <ds-link
                @click.prevent="remove(file)" styling="default"
              >
                Remove
              </ds-link>
            </div>
          </cell>

          <cell
            cols="12 6-sm 4-md"
            v-for="file in uploader.accepted"
            :key="file.id"
            @click.stop=""
          >
            <div>{{ file.file.name }}</div>
            <div>
              <ds-link
                @click.prevent="uploader.remove(file)" styling="default"
              >
                Remove
              </ds-link>
              <ds-link
                v-if="file.status === 'ready'"
                @click.prevent="uploader.upload(file)" styling="default"
              >
                Start upload
              </ds-link>
            </div>
            <div>
              <progress max="1" :value="file.progress.relative" style="height: 0.25rem"/>
            </div>
          </cell>
        </row>
      </div>
    </template>
  </vue-dropzone-uploader>
</template>
<script>
export default {
  data() {
    return {
      value: [],
    };
  },
  methods: {
    uploadHandler({ result, descriptor }) {
      this.$refs.uploader.remove(descriptor);

      this.input(this.value.concat([result]));
    },
    input(data) {
      this.value = data;
    },
    open() {
      this.$refs.uploader.open();
    },
    remove(file) {
      const { id } = file;

      const index = this.value.findIndex(x => x.id === id);
      if (index !== -1) {
        this.input(this.value.slice(0, index).concat(this.value.slice(index + 1)));
      }
    },
  },
};
</script>
<style>
.vue-dropzone__input { display: none; }
</style>
```
:::
