# Vue Dropzone component

Simple dropzone component for vue. Based on `@aspectus/dropzone` implementation.

## Installation

```
yarn add @aspectus/vue-dropzone
```

```js
import VueDropzone from '@aspectus/vue-dropzone';

Vue.use(VueDropzone);
// Or
Vue.use(VueDropzone, {
  name: 'v-dropzone',
});
```

## Usage

::: warning No styling!
This package does not provide any styling, so you should add the base styling by your own.
:::

Props:

- `block-name`: Custom block name for your element,
- `multiple`: Ability to add one or multiple files at a time.
- `config`: Configuration object for underlying `@aspectus/dropzone`.
- Available all additional styling props [from `@aspectus/vue-bem-styling`](vue-bem-styling.html).

### Basic

::: demo
```html
<template>
  <vue-dropzone style="position: relative" @drop="fileDrop">
    <section class="t-content">
      <p>Here you may upload anything.</p>
      <p>The result will be prompted to console.</p>
    </section>
  </vue-dropzone>
</template>
<script>
export default {
  methods: {
    fileDrop(event) {
      console.log(event);
    }
  }
};
</script>
<style>
.vue-dropzone__input { display: none; }
</style>
```
:::

### With simple dragging cover message

::: demo
```html
<template>
  <vue-dropzone style="position: relative" @drop="fileDrop">
    <template slot="cover">
      <div
        style="
          position: fixed;
          top: 0;
          left: 0;
          height: 100%;
          width: 100%;
          opacity: 0.9;
          background-color: #eee;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          pointer-events: none;
        "
      >
        <div class="ds-caption ds-caption--size_6 ds-caption--variant_secondary ds-caption--variant_uppercase">
          Trying to upload?
        </div>
        <div class="ds-caption ds-caption--size_3">
          Files, I suppose?
        </div>
      </div>
    </template>
    <section class="t-content">
      <p>Here you may upload anything.</p>
      <p>The result will be prompted to console.</p>
    </section>
  </vue-dropzone>
</template>
<script>
export default {
  methods: {
    fileDrop(event) {
      console.log(event);
    }
  }
};
</script>
<style>
.vue-dropzone__input { display: none; }
</style>
```
:::

### With custom block name

::: demo
```html
<template>
  <vue-dropzone style="position: relative" @drop="fileDrop" block-name="da-dropzone">
    <template slot="cover">
      <div
        style="
          position: fixed;
          top: 0;
          left: 0;
          height: 100%;
          width: 100%;
          opacity: 0.9;
          background-color: #eee;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          pointer-events: none;
        "
      >
        <div class="ds-caption ds-caption--size_6 ds-caption--variant_secondary ds-caption--variant_uppercase">
          Trying to upload?
        </div>
        <div class="ds-caption ds-caption--size_3">
          Files, I suppose?
        </div>
      </div>
    </template>
    <section class="t-content">
      <p>Here you may upload anything.</p>
      <p>The result will be prompted to console.</p>
    </section>
  </vue-dropzone>
</template>
<script>
export default {
  methods: {
    fileDrop(event) {
      console.log(event);
    }
  }
};
</script>
<style>
.da-dropzone__input { display: none; }
</style>
```
:::

### With benefits from `@aspectus/vue-bem-styling`

::: demo
```html
<template>
  <vue-dropzone style="position: relative" @drop="fileDrop" block-name="da-dropzone" variant="solo" theme="bootstrap" disabled>
    <template slot="cover">
      <div
        style="
          position: fixed;
          top: 0;
          left: 0;
          height: 100%;
          width: 100%;
          opacity: 0.9;
          background-color: #eee;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          pointer-events: none;
        "
      >
        <div class="ds-caption ds-caption--size_6 ds-caption--variant_secondary ds-caption--variant_uppercase">
          Trying to upload?
        </div>
        <div class="ds-caption ds-caption--size_3">
          Files, I suppose?
        </div>
      </div>
    </template>
    <section class="t-content">
      <p>Here you may upload anything.</p>
      <p>The result will be prompted to console.</p>
    </section>
  </vue-dropzone>
</template>
<script>
export default {
  methods: {
    fileDrop(event) {
      console.log(event);
    }
  }
};
</script>
<style>
.da-dropzone__input { display: none; }
</style>
```
:::
