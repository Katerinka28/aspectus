# Vue loading state mixin

Vue mixin to control internal loading state.

## Installation

```
yarn add @aspectus/vue-loading-state-mixin
```

## Usage

Adds `loading` boolean data variable.

### Emits

- `loading-started` - Loading process started.
- `loading-finished` - Loading process finished.

### Methods

- `startLoading` - Increases loading processes count.
- `stopLoading` - Decreases loading processes count.
- `$load(promise: Promise)` - Calls `startLoading` and `endLoading` methods on promise execution states change.

```js
import LoadingStateMixin from '@aspectus/vue-loading-state-mixin';

export default {
  mixins: [LoadingStateMixin],
  props: ['promise1', 'promise2'],

  created() {
    this.$load(this.promise1);
    this.$load(this.promise2);
  }
};
```
