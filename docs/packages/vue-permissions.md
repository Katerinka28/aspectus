# Vue permission checker components

Components for permissions usage in vue.

## Installation

```sh
yarn add @aspectus/vue-permissions
```

## Usage

::: demo
```html
<template>
  <div>
    <div>
      <permissions-access-state :permissions="[available]" v-slot="{ hasAccess }">
        {{ hasAccess ? 'Always display' : 'Newer will be displayed' }}
      </permissions-access-state>
    </div>

    There are 4 components, but only 2 will be displayed:

    <permissions-available :permissions="[available]">
      One
    </permissions-available>
    <permissions-available :permissions="[notAvailable]">
      Hidden
    </permissions-available>
    <permissions-available :permissions="[new $testPermissions.And(notAvailable, available)]">
      <div>Hidden 2</div>
    </permissions-available>
    <permissions-available tag="span" :permissions="[new $testPermissions.Or(notAvailable, available)]">
      <span>T</span><span>wo</span>
    </permissions-available>
  </div>
</template>
<script>
export default {
  computed: {
    available() {
      return new this.$testPermissions.AlwaysTrue();
    },
    notAvailable() {
      return new this.$testPermissions.AlwaysFalse();
    }
  }
};
</script>
```
:::
