# Vue route installer

Vue utility to lazily install page/route related plugins.

## Installation

```
yarn add @aspectus/vue-route-installer
```

## Usage

```js
import vueRouterInstaller from '@aspectus/vue-router-installer';

const RouteInstaller = () => import('./modules/some-page/components');
const RouteView = () => import('./modules/som-page/View');

export function createRoutes({ Vue }) {
  const routes = [
    {
      name: 'some-page',
      path: 'some-page/',
      component: vueRouterInstaller(RouteView, RouteInstaller, Vue),
    },
  ];

  return routes;
}
```

Module `RouteInstaller` from this example must provide vue provider's interface to be able to be installed before route execution.
