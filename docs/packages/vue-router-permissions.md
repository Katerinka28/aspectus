# Vue router permissions

Mechanics for applying permissions check for vue router views and links.

Based on `@aspectus/vue-permissions` and `@aspectus/permissions` packages.

## Installation

```sh
yarn add @aspectus/vue-router-permissions
```

## Usage

### Router

Router checker can be added as simple as that:

```js
// router.js
// Global access guard is enough to check for permissions in any view.

import { Or } from '@aspectus/permissions';
import {
  accessGuard, accessMeta, accessConfig,
} from '@aspectus/vue-router-permissions';

// ...

const routes = [
  // Simple protected route:
  {
    // ...
    meta: accessMeta(new CustomHasPermissionChecker('some-permission'))
  },

  // These three variants have the same result:
  {
    // ...
    meta: {
      accessConfig: {
        // Permission to check
        checker: new Or(
          new CustomHasPermissionChecker('some-permission'),
          new CustomIsSuperuserChecker()
        ),
        // Redirect, by default(`false`) just stops view resolving
        redirect: { name: 'auth:login' },
        // Additional options for checker execution might be added here:
        // ...
      },
    },
  },
  {
    // ...
    meta: {
      accessConfig: accessConfig(
        // Permission to check
        new Or(
          new CustomHasPermissionChecker('some-permission'),
          new CustomIsSuperuserChecker()
        ),
        // Redirect, by default(`false`) just stops view resolving
        { name: 'auth:login' },
        // Additional options for checker execution
        {}
      ),
    },
  },
  // Or access control can be added via shortcut:
  {
    // ...
    meta: accessMeta(
      // Permission to check
      new Or(
        new CustomHasPermissionChecker('some-permission'),
        new CustomIsSuperuserChecker()
      ),
      // Redirect
      { name: 'auth:login' },
      // Additional options for checker execution
      {}
    ),
  },
];

const router = new Router({
  // ...
  routes,
});

router.beforeEach(accessGuard(
  /* Additional parameters that will be passed to all permission checkers */
  {}
));
```

### Templates

```html
<template>
  <div>
    <route-available :to="{ name: 'some:where', params: { id: item.id } }">
      Only shows something(like `router-link`), when checkers on the route,
      where `:to` parameter leads, is passed as truthful.
    </route-available>

    <route-access-state
      :to="{ name: 'some:where', params: { id: item.id } }"
      v-slot="{ hasAccess }"
    >
      Same as previous, but it just provides {{ hasAccess }} prop and you
      will decide what to do with it.
    </route-access-state>
  </div>
</template>
```
