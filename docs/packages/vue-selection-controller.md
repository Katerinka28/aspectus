# Vue SelectionController component

Simple selection-controller component for vue. Based on `@aspectus/selection-controller` implementation.

## Installation

```
yarn add @aspectus/vue-selection-controller
```

```js
import VueSelectionController from '@aspectus/vue-selection-controller';

Vue.use(VueSelectionController);
// Or
Vue.use(VueSelectionController, {
  name: 'v-selection-controller',
});
```

## Usage

Props:

- `multiple`: Whether the value could be multiple or not.
- `options`: Available options.
- `value`: Current value. Supports default v-model interface.
- `states`: Map with additional checker functions.
- `key-getter`: Key getter function for `@aspectus/selection-controller` usage.

Slot:

- `change`: Function to call for value change.
- `multiple`: Same as appropriate prop.
- `keyGetter`: Same as appropriate prop.
- `value`: Normalized value.
- `options`: Normalized options list with additional checkers.

::: demo
```html
<template>
  <div>
    <label style="cursor: pointer">
      <span class="ds-inliner">
        <span class="ds-inliner__body">
          <input type="checkbox" v-model="multiple" />
        </span>
      </span>
      Should it be multiple?
    </label>
    <br/><br/>
    <vue-selection-controller
      :options="options"
      :multiple="multiple"
      :key-getter="$options.keyGetter"
      :states="{ disabled: $options.disabled }"
      v-model="value"
      v-slot="controller"
    >
      <div>
        <div>
          Current value:
          <span v-for="v in controller.value"> {{ v.display }} </span>
        </div>
        <br/>
        <ul class="t-content">
          <li
            style="cursor: pointer"
            v-for="option in controller.options"
            :key="option.key"
            @click.prevent="
              !option.disabled
                ? controller.change(option.value)
                : null
            "
          >
            {{ option.value.display }} {{ option.selected && '[selected]' || '' }} {{ option.disabled && '[disabled]' || '' }}
          </li>
        </ul>
      </div>
    </vue-selection-controller>
  </div>
</template>
<script>
export default {
  keyGetter: x => x.id,
  disabled: x => x.id === 2,

  data() {
    return {
      multiple: false,
      value: null,
      options: [
        { id: null, display: 'Nothing selected' },
        { id: 1, display: 'First one' },
        { id: 2, display: 'Second one' },
        { id: 3, display: 'Third one' },
      ]
    };
  },
};
</script>
```
:::
