# Vue `tag` component

Component for custom tag usage inside components. It's used to replace vue built in `:is` mechanism because it's working in a slightly different way(do not know whether it's a bug or a feature).

## Installation

```
yarn add @aspectus/vue-tag
```

```js
import TagPlugin from '@aspectus/vue-tag';

Vue.use(TagPlugin);
// Or
Vue.use(TagPlugin, { name: 'custom-tag-component-name' });
```

## Usage

Use in your component as a root element to be able to change it's tag on the fly.

```html
<!-- SomeComponent -->
<template>
  <tag>
    Some component internals.
  </tag>
</template>
```

And then use it in your application:
```html
<SomeComponent tag="h1" />
```

You can pass any data that fits vue's "component/element" behaviour into `tag`. It could be string(regular html tag, name of the registered component, name of the internal component) or an vue component object definition.
