# Vue utils

Vue related utilities that makes development easier.

## Installation

```
yarn add @aspectus/vue-utils
```

## Usage

### `renderSlim(nodes: Array<VNode>, h: Function, tag = 'div', data: VueContext)`

Function, that renders nodes inside of a provided tag, but if there are only one node - returns just it. Mostly used by renderless components, when it's not necessarily needed to wrap children into a tag.

```js
{
  render(h) {
    return renderSlim(this.$scopedSlots.default({ value: this.value }));
  }
}
```

### `mergeContext(...context: Array<VueContext>)`

Function that merges any number of vue functional contexts from left to right.

### `resolveInjection(key: string, vm)`

Searches for a vue injection, as a regular vue `inject` does. Just because vue's `inject` can not have a dynamic names, for example received from some provided prop - this function makes it easy to be able to get any injection that component requires.

```js
{
  props: ['name'],
  computed: {
    injection() {
      return resolveInjection(this.name, this);
    },
  },
}
```
