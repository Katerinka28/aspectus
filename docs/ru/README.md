---
lang: ru-RU
home: true
description: Набор утилит для разработки приложения
heroImage: /logo.svg
actionText: Начнем »
actionLink: /packages/bem.html
footer: MIT Licensed | Copyright © 2019-сегодня Алексей Ткаченко
meta:
  - name: og:title
    content: Aspectus
  - name: og:description
    content: Набор утилит для разработки приложения
---
