import { preparePairs, isString } from './utils';
import {
  NAMESPACE, ELEMENT_DELIMITER, MODIFIER_DELIMITER, VALUE_DELIMITER,
} from './const';

export function generateModifierClassNames(className, modifiers = {}, m, v) {
  const prepared = preparePairs(modifiers)
    .map(modifier => modifier.join(v));
  const prefix = `${className}${m}`;

  return prepared.map(modifier => prefix + modifier);
}

export function generateElementClassName(block, element = null, n = false, e) {
  const stack = [(!n ? '' : n) + block];

  if (element) {
    stack.push(element);
  }

  return stack.join(e);
}

export function generateClassNames(block, element = null, modifiers = {}, { n, e, m, v }) {
  const className = generateElementClassName(block, element, n, e);
  const modifierClassNames = generateModifierClassNames(className, modifiers, m, v);

  return [className].concat(modifierClassNames);
}

export class BlockGenerator {
  constructor({
    n = NAMESPACE, e = ELEMENT_DELIMITER,
    m = MODIFIER_DELIMITER, v = VALUE_DELIMITER,
  } = {}) {
    this.config = { n, e, m, v };
  }

  block(block, element, modifiers = {}) {
    return generateClassNames(block, element, modifiers, this.config);
  }
}

export function createBlockNameGenerator(config) {
  const generator = new BlockGenerator(config);

  return (block, eom, modifiers) => generator.block(
    block,
    isString(eom) ? eom : undefined,
    modifiers || !isString(eom) && eom || {}
  );
}

export function createBlockGenerator(config) {
  const generator = createBlockNameGenerator(config);

  return (block, element) => (eom, modifiers) => generator(
    block, isString(eom) ? eom : element, modifiers || !isString(eom) && eom || {}
  );
}
