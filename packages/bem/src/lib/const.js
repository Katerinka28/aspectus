export const NAMESPACE = '';
export const ELEMENT_DELIMITER = '__';
export const MODIFIER_DELIMITER = '--';
export const VALUE_DELIMITER = '_';

export const STATE_PREFIX = 'is-';
