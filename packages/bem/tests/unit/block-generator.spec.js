import { BlockGenerator, createBlockNameGenerator } from '@/main';

describe('Block generator', () => {
  it('Creates', () => {
    expect(() => new BlockGenerator()).not.toThrow();
    expect(() => createBlockNameGenerator()).not.toThrow();
  });

  it('Generates from default config', () => {
    const blockGenerator = createBlockNameGenerator();

    const blockFull = blockGenerator('block', {
      some: true, other: 'value', disabled: false, multiple: ['one', 'two'],
    });
    expect(blockFull).toContain('block');
    expect(blockFull).toContain('block--some');
    expect(blockFull).toContain('block--other_value');
    expect(blockFull).not.toContain('block--disabled');
    expect(blockFull).toContain('block--multiple_one');
    expect(blockFull).toContain('block--multiple_two');
    expect(blockFull).toHaveLength(5);

    const elementFull = blockGenerator('block', 'element', {
      some: true, other: 'value', disabled: false, multiple: ['one', 'two'],
    });
    expect(elementFull).toContain('block__element');
    expect(elementFull).toContain('block__element--some');
    expect(elementFull).toContain('block__element--other_value');
    expect(elementFull).not.toContain('block__element--disabled');
    expect(elementFull).toContain('block__element--multiple_one');
    expect(elementFull).toContain('block__element--multiple_two');
    expect(elementFull).toHaveLength(5);

    const elementOnly = blockGenerator('block', 'element');
    expect(elementOnly).toContain('block__element');
    expect(elementOnly).toHaveLength(1);

    const elementOnly2 = blockGenerator('block', 'element', {
      all: false, are: false, disabled: false,
    });
    expect(elementOnly2).toContain('block__element');
    expect(elementOnly2).toHaveLength(1);
  });

  it('Generates from custom config', () => {
    const blockGenerator = createBlockNameGenerator({
      n: 'p-', e: '-', m: '_', v: '--',
    });

    const blockFull = blockGenerator('block', {
      some: true, other: 'value', disabled: false, multiple: ['one', 'two'],
    });
    expect(blockFull).toContain('p-block');
    expect(blockFull).toContain('p-block_some');
    expect(blockFull).toContain('p-block_other--value');
    expect(blockFull).not.toContain('p-block_disabled');
    expect(blockFull).toContain('p-block_multiple--one');
    expect(blockFull).toContain('p-block_multiple--two');
    expect(blockFull).toHaveLength(5);

    const elementFull = blockGenerator('block', 'element', {
      some: true, other: 'value', disabled: false, multiple: ['one', 'two'],
    });
    expect(elementFull).toContain('p-block-element');
    expect(elementFull).toContain('p-block-element_some');
    expect(elementFull).toContain('p-block-element_other--value');
    expect(elementFull).not.toContain('p-block-element_disabled');
    expect(elementFull).toContain('p-block-element_multiple--one');
    expect(elementFull).toContain('p-block-element_multiple--two');
    expect(elementFull).toHaveLength(5);

    const elementOnly = blockGenerator('block', 'element');
    expect(elementOnly).toContain('p-block-element');
    expect(elementOnly).toHaveLength(1);

    const elementOnly2 = blockGenerator('block', 'element', {
      all: false, are: false, disabled: false,
    });
    expect(elementOnly2).toContain('p-block-element');
    expect(elementOnly2).toHaveLength(1);
  });
});
