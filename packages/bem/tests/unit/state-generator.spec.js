import { StateGenerator, createStateGenerator } from '@/main';

describe('State generator', () => {
  it('Creates', () => {
    expect(() => new StateGenerator()).not.toThrow();
    expect(() => createStateGenerator()).not.toThrow();
  });

  it('Generates from default config', () => {
    const state = createStateGenerator();

    expect(state({ some: true })).toContain('is-some');

    const compound = state({ some: 'value', other: true, disabled: false });

    expect(compound).toContain('is-other');
    expect(compound).not.toContain('is-disabled');
    expect(compound).not.toContain('is-some');
    expect(compound).toContain('is-some_value');
    expect(compound).toHaveLength(2);
  });

  it('Generates from custom config', () => {
    const state = createStateGenerator({ sp: 'state-', v: '--' });

    expect(state({ some: true })).toContain('state-some');

    const compound = state({ some: 'value', other: true, disabled: false });

    expect(compound).toContain('state-other');
    expect(compound).not.toContain('state-disabled');
    expect(compound).not.toContain('state-some');
    expect(compound).toContain('state-some--value');
    expect(compound).toHaveLength(2);
  });
});
