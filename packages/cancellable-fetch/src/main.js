/* eslint-disable prefer-object-spread */

import cancellablePromiseProxy from '@aspectus/cancellable-promise-proxy';

export default function fetcher(resource, init = {}) {
  if ('signal' in init) {
    return fetch(resource, init);
  }

  const cancelController = new AbortController();

  return cancellablePromiseProxy(
    (resolve, reject) => {
      fetch(
        resource,
        Object.assign({}, init, { signal: cancelController.signal })
      )
        .then(resolve, reject);
    },
    { cancelController }
  );
}
