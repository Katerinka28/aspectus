/* eslint-disable prefer-rest-params */

export default class CancellablePromise extends Promise {
  constructor(resolver, controller = null) {
    super(resolver);
    this.controller = controller;
  }

  then() {
    const result = super.then.apply(this, arguments);
    result.controller = this.controller;
    return result;
  }

  catch() {
    const result = super.catch.apply(this, arguments);
    result.controller = this.controller;
    return result;
  }

  cancel() {
    if (this.controller && this.controller.abort) {
      this.controller.abort();
    }
  }
}
