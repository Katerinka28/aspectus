/* eslint-disable */
/**
 * Module is deprecated in favor of `resize` handler. But it's not finale
 * because of performance. Might be changed.
 * Even so - saved here as an archived.
 */

export const EQ_BLOCK = 'u-eq';
export const EQ_DEFAULT_PREFIX = `${EQ_BLOCK}--`;
export const EQ_DEFAULT_BREAKPOINTS = [
  '>260', '<260',
  '>390', '<390',
  '>520', '<520',
  '>780', '<780',
  '>1040', '<1040',
];
export const EQ_BREAKPOINT_REGEX = /(.+?)([0-9]+)/i;

function remove(node) {
  return node && node.parentNode && node.parentNode.removeChild(node) || null;
}

export function parseBreakpoint(breakpoint) {
  const [key, comparator, value] = breakpoint.match(EQ_BREAKPOINT_REGEX);

  return { key, comparator, value: parseFloat(value) };
}

export class EqFunctor {
  constructor(options) {
    this.update(options);
  }

  update({
    breakpoints = this.breakpoints,
  } = {}) {
    this.breakpoints = breakpoints || [];

    this.triggers = this.breakpoints.map(parseBreakpoint);
  }
}

export class EqObservable {
  constructor({ block = EQ_BLOCK, triggers = [] } = {}) {
    this.triggers = [];
    this.triggerElements = [];

    this.createElement = this.createElement.bind(this);
    this.createTrigger = this.createTrigger.bind(this);
    this.block = block;

    this.update({ triggers });
  }

  update({ triggers = this.triggers } = {}) {
    const del = this.triggers.map(x => x.key).filter(t => !triggers.some(x => t === x.key));
    const add = triggers.filter(t => !this.triggers.some(x => t.key === x.key));
    this.triggers = triggers;
    this.triggerElements.filter(x => del.includes(x.dataset.eqKey)).map(remove);

    if (!this.element) {
      this.element = this.createElement();
    }

    this.triggerElements = add.map(this.createTrigger);
    this.triggerElements.forEach(element => this.element.appendChild(element));
  }

  createElement() {
    const element = document.createElement('div');
    element.className = `${this.block}__observable`;

    return element;
  }

  createTrigger({ key, comparator, value }) {
    const element = document.createElement('div');
    element.dataset.eqComparator = comparator;
    element.dataset.eqKey = key;
    element.className = `${this.block}__trigger`;
    element.style.width = `${value}px`;

    return element;
  }
}

export class EqObserver {
  constructor({
    block = EQ_BLOCK,
    prefix = EQ_DEFAULT_PREFIX,
    breakpoints = EQ_DEFAULT_BREAKPOINTS,
  } = {}) {
    this.block = block;
    this.handleChange = this.handleChange.bind(this);

    this.functor = new EqFunctor();
    this.observable = new EqObservable({ block });

    this.update({ prefix, breakpoints });
  }

  handleChange(entries) {
    entries.forEach(entry => {
      const key = entry.target.dataset.eqKey;
      const comparator = entry.target.dataset.eqComparator;
      const gt = comparator === '>';
      const intersect = entry.intersectionRatio === 1;
      const className = `${this.prefix}${key}`;
      const action = gt === intersect ? 'add' : 'remove';

      this.el.classList[action](className);
    });
  }

  update({
    prefix = this.prefix,
    ...rest
  } = {}) {
    const { el } = this;

    if (el) this.observable.triggerElements.forEach(trigger => this.observer.unobserve(trigger));

    this.prefix = prefix;

    this.functor.update(rest);
    this.observable.update({ triggers: this.functor.triggers });

    if (el) this.observable.triggerElements.forEach(trigger => this.observer.observe(trigger));
  }

  bind(el) {
    if (!el || !('IntersectionObserver' in window)) {
      return this;
    }

    this.el = el;
    this.el.classList.add(this.block);

    this.observer = new IntersectionObserver(
      this.handleChange,
      { root: this.observable.element, rootMargin: '0px', threshold: 1 }
    );
    this.observable.triggerElements.forEach(trigger => this.observer.observe(trigger));
    this.el.appendChild(this.observable.element);

    return this;
  }

  unbind() {
    if (!this.el) return;

    this.observable.triggerElements.forEach(trigger => this.observer.unobserve(trigger));
    this.el.removeChild(this.observable.element);
    this.el.classList.remove(this.block);

    delete this.el;

    return this;
  }
}
