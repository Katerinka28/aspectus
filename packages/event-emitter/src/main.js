export default class EventEmitter {
  constructor() {
    this.callbacks = {};
  }

  on(event, fn) {
    if (!this.callbacks[event]) {
      this.callbacks[event] = [];
    }

    this.callbacks[event].push(fn);

    return this;
  }

  emit(event, ...args) {
    const callbacks = this.callbacks[event];

    if (callbacks) {
      const length = callbacks.length; // eslint-disable-line prefer-destructuring

      for (let i = 0; i < length; i++) {
        callbacks[i].apply(this, args);
      }
    }

    return this;
  }

  off(event, fn) {
    if (!this.callbacks || (arguments.length === 0)) {
      this.callbacks = {};

      return this;
    }

    const callbacks = this.callbacks[event];
    if (!callbacks) {
      return this;
    }

    if (arguments.length === 1) {
      delete this.callbacks[event];

      return this;
    }

    for (let i = 0; i < callbacks.length; i++) {
      if (callbacks[i] === fn) {
        callbacks.splice(i, 1);

        break;
      }
    }

    return this;
  }
}
