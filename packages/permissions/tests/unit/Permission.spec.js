/* eslint-disable max-classes-per-file, class-methods-use-this */

import { Permission, Or, And } from '@/main';

// `Promise.any` polyfill
const reverse = p => new Promise((resolve, reject) => Promise.resolve(p).then(reject, resolve));
Promise.any = arr => reverse(Promise.all(arr.map(reverse)));

class AlwaysTrue extends Permission { }

class AlwaysFalse extends Permission {
  hasAccess() { return false; }
}

class PassedReturn extends Permission {
  hasAccess(value) { return value; }
}

describe('Permission', () => {
  it('Inherits', async () => {
    let toResolve = true;

    class VariativelyAccessiblePermission extends Permission {
      hasAccess() {
        return toResolve;
      }
    }

    const varAccessible = new VariativelyAccessiblePermission();

    await expect(varAccessible.onHasAccess()).resolves.toBeUndefined;
    toResolve = false;
    await expect(varAccessible.onHasAccess()).rejects.toBeUndefined;
    toResolve = true;
    await expect(varAccessible.onHasAccess()).resolves.toBeUndefined;
  });

  it('Simple collections', async () => {
    const or = new Or(new AlwaysFalse(), new AlwaysTrue());
    const or2 = new Or(new AlwaysTrue(), new AlwaysFalse());
    const or3 = new Or(new AlwaysFalse(), new AlwaysFalse());

    const and = new And(new AlwaysFalse(), new AlwaysTrue());
    const and2 = new And(new AlwaysTrue(), new AlwaysFalse());
    const and3 = new And(new AlwaysTrue());
    const and4 = new And(new AlwaysTrue(), new AlwaysTrue());

    await expect(or.onHasAccess()).resolves.toBeUndefined();
    await expect(or2.onHasAccess()).resolves.toBeUndefined();
    await expect(or3.onHasAccess()).rejects.toBeDefined();

    await expect(and.onHasAccess()).rejects.toBeUndefined();
    await expect(and2.onHasAccess()).rejects.toBeUndefined();
    await expect(and3.onHasAccess()).resolves.toBeDefined();
    await expect(and4.onHasAccess()).resolves.toBeDefined();
  });

  it('Complex logic', async () => {
    const resolvable = new Or(
      new And(
        new Or(
          new AlwaysTrue(),
          new AlwaysFalse()
        ),
        new Or(
          new AlwaysFalse(),
          new PassedReturn()
        )
      ),
      new Or(
        new AlwaysFalse(),
        new And(new AlwaysTrue(), new AlwaysFalse())
      )
    );

    const rejectable = new Or(
      new And(
        new Or(
          new AlwaysTrue(),
          new AlwaysFalse()
        ),
        new Or(
          new AlwaysFalse(),
          new And(new AlwaysTrue(), new AlwaysFalse())
        )
      ),
      new Or(
        new AlwaysFalse(),
        new And(new AlwaysTrue(), new AlwaysFalse())
      )
    );

    await expect(resolvable.onHasAccess(true)).resolves.toBeDefined();
    await expect(resolvable.onHasAccess(false)).rejects.toBeDefined();
    await expect(rejectable.onHasAccess()).rejects.toBeDefined();
  });
});
