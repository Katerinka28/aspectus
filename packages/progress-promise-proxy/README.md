# Progress Promise proxy API

Simple Promise proxy that adds `.progress` handler registry to promise object.

[Documentation](https://preusx.gitlab.io/aspectus/packages/progress-promise-proxy.html)
