/*
  eslint-disable
    no-return-assign,
    prefer-spread,
    prefer-rest-params,
    prefer-promise-reject-errors
*/

import compose from 'ramda/src/compose';
import { createProxy, composeProxies } from '@/main';

describe('Proxy', () => {
  it('Creates composed', () => {
    expect(() => composeProxies({ create() {} })).not.toThrow();
  });

  it('No data execution', async () => {
    const ChainAliasProxy = {
      chain() {
        return this.then.apply(this, arguments);
      },
    };

    const ErrorAliasProxy = createProxy({
      error() {
        return this.catch.apply(this, arguments);
      },
    });

    const JustProxy = {
      some() {
        return this.then.apply(this, arguments);
      },
    };

    const proxy = composeProxies(ChainAliasProxy, ErrorAliasProxy);
    const proxy2 = composeProxies(proxy, JustProxy);

    await proxy(Promise.resolve(20)).then(x => x).chain(result => expect(result).toBe(20));
    await proxy(Promise.reject(20)).error(result => expect(result).toBe(20));

    await proxy2(Promise.resolve(20)).then(x => x).chain(result => expect(result).toBe(20));
    await proxy2(Promise.reject(20)).error(result => expect(result).toBe(20));
    await proxy2(Promise.resolve(20)).then(x => x).some(result => expect(result).toBe(20));
  });

  it('With data execution', async () => {
    const ReturnProxy = {
      return() {
        return this.$data.some;
      },
    };

    // Don't do like that! Or you must be VERY careful.
    const CalculateProxy = createProxy({
      then(callback) {
        return this.$apply('then', [value => callback.call(this.$proxy, value * this.$data.factor)]);
      },
    });

    const proxy = composeProxies(ReturnProxy, CalculateProxy);

    const thened = proxy(Promise.resolve(20), { some: 30, factor: 2 }).then(x => x);
    expect(thened.return()).toBe(30);

    await thened;
    await proxy(Promise.resolve(20), { some: 30, factor: 2 })
      .then(result => expect(result).toBe(40));
  });

  it('With data execution and ramda\'s compose', async () => {
    const ReturnProxy = createProxy({
      return() {
        return this.$data.some;
      },
    });

    // Don't do like that! Or you must be VERY careful.
    const CalculateProxy = createProxy({
      then(callback) {
        return this.$apply('then', [value => callback.call(this.$proxy, value * this.$data.factor)]);
      },
    });

    const proxy = compose(CalculateProxy, ReturnProxy);

    const thened = proxy(Promise.resolve(20), { some: 30, factor: 2 }).then(x => x);
    expect(thened.return()).toBe(30);

    await thened;
    await proxy(Promise.resolve(20), { some: 30, factor: 2 })
      .then(result => expect(result).toBe(40));
  });
});
