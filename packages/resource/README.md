# Fetch API wrapper `resource`

Composable fetch API interface.

[Documentation](https://preusx.gitlab.io/aspectus/packages/resource.html)
