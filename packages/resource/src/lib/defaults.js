import { ResourceChain } from './chain';

export const baseResource = new ResourceChain()
  .config({
    mode: 'same-origin',
    cache: 'default',
    credentials: 'same-origin',
    redirect: 'follow',
    referrer: 'client',
  });

export const receiveResource = baseResource
  .config({
    method: 'GET',
  });

export const sendResource = baseResource
  .config({
    method: 'POST',
  });
