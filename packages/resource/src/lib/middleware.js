import curry from 'ramda/src/curry';
import pipe from 'ramda/src/pipe';

export const keyMiddleware = curry((key, partialMiddleware, params) => ({
  ...params,
  [key]: partialMiddleware((params || {})[key]),
}));
export const configMiddleware = keyMiddleware('config');
export const parametersMiddleware = keyMiddleware('parameters');
export const headersMiddleware = keyMiddleware('headers');
export const bodyMiddleware = keyMiddleware('body');

export const defaultsMiddleware = defaults => parameters => ({ ...defaults, ...parameters });
export const overrideMiddleware = override => parameters => ({ ...parameters, ...override });

export const jsonRequestMiddleware = pipe(
  headersMiddleware(overrideMiddleware({ 'Content-Type': 'application/json' })),
  bodyMiddleware(body => (
    typeof body === 'undefined' || body === null
      ? body :
      JSON.stringify(body)
  ))
);
