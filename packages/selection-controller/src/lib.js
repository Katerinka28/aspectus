/* eslint-disable prefer-destructuring */

export const toArray = value => (Array.isArray(value) ? value : [value]);
export const defaultKeyGetter = v => v;
export const isUndefined = value => typeof value === 'undefined';
export const isValue = value => (!isUndefined(value) && value !== null);

export function getKey(value, keyGetter = defaultKeyGetter) {
  if (isValue(value)) {
    const key = keyGetter(value);

    if (!isUndefined(key)) {
      return key;
    }

    return value;
  }

  return null;
}

const getOption = (options, key, dflt) => {
  const option = options.find(x => x.key === key);

  return option && option.value || dflt;
};
const constructKeys = (options, keyGetter) => options.map(x => getKey(x, keyGetter));

export function resolveStates(checkers, option, key, selected) {
  const states = {};
  const keys = Object.keys(checkers);

  for (let i = 0, length = keys.length; i < length; i++) {
    states[keys[i]] = checkers[keys[i]](option, key, selected);
  }

  return states;
}

export function prepareOptions(options, value, keyGetter, stateCheckers = {}) {
  const keys = constructKeys(toArray(value), keyGetter);

  return options.map(option => {
    const key = getKey(option, keyGetter) || null;
    const selected = keys.findIndex(x => x === key) !== -1;
    const states = resolveStates(stateCheckers, option, key, selected);

    return Object.assign(states, { key, selected, value: option });
  });
}

export function prepareValue(options, value, keyGetter) {
  return toArray(value)
    .map(v => getOption(options, getKey(v, keyGetter), v));
}

export function prepareData(elements, current, keyGetter, stateCheckers = {}) {
  const value = toArray(current);
  const options = prepareOptions(elements, value, keyGetter, stateCheckers);

  return {
    options,
    value: prepareValue(options, value, keyGetter),
  };
}

export function updateValue(needle, current, keyGetter, multiple = false) {
  let value = needle;
  const key = getKey(needle, keyGetter);

  if (!isValue(key) && !Array.isArray(needle)) return [null];

  if (!Array.isArray(needle)) {
    if (!multiple || !needle) {
      value = [needle];
    } else {
      value = current.slice();
      const index = value.findIndex(x => getKey(x, keyGetter) === key);

      if (index !== -1) {
        value.splice(index, 1);
      } else {
        value.push(needle);
      }

      value = value.filter(x => isValue(x) && isValue(getKey(x, keyGetter)));
    }
  }

  if (value.length === 0) {
    value = [null];
  }

  return value;
}

export function resultValue(value, multiple = false) {
  return multiple ? value : value[0];
}

export default {
  resolveStates,
  prepareOptions,
  prepareValue,
  prepareData,
  updateValue,
  resultValue,
};
