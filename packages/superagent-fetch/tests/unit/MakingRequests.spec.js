/*
  eslint-disable
    no-underscore-dangle
*/

import { ResourceChain, headersMiddleware, defaultsMiddleware } from '@aspectus/resource';
import turl from '@aspectus/resource-template-url-getter';
import fetcher from '@/main';

const { WITH_REQUESTS } = process.env;

describe('Making requests', () => {
  const base = new ResourceChain()
    .fetcher(fetcher)
    .middleware(headersMiddleware(defaultsMiddleware({
      Accept: 'application/json',
    })));

  it('Just executes', async () => {
    if (!WITH_REQUESTS) return Promise.resolve();

    const resource = base.url(turl('https://jsonplaceholder.typicode.com/posts{/id}'));
    const request = resource.execute({ id: 1 });

    expect(request).resolves.toMatchObject({
      status: 200,
    });

    const result = await request;
    expect(JSON.parse(result.text)).toMatchObject({ id: 1 });
    expect(result.req._header).toEqual(expect.stringContaining('Accept: application/json'));

    return result;
  });

  it('Sends data', async () => {
    if (!WITH_REQUESTS) return Promise.resolve();

    const resource = base.url(turl('https://jsonplaceholder.typicode.com/posts'));
    const request = resource
      .config('method', 'POST')
      .middleware(headersMiddleware(defaultsMiddleware({
        'Content-type': 'application/json; charset=UTF-8',
      })))
      .execute({}, JSON.stringify({
        title: 'foo',
        body: 'bar',
        userId: 100500,
      }));

    expect(request).resolves.toMatchObject({
      status: 201,
    });

    const result = await request;
    expect(JSON.parse(result.text)).toMatchObject({ userId: 100500 });

    return result;
  });
});
