/* eslint-disable import/extensions, import/no-extraneous-dependencies */
import Vue from 'vue';
import '@dermis/dermis';

import { extend, ValidationObserver } from 'vee-validate';
import { required, min } from 'vee-validate/dist/rules';
import TagPlugin from '@aspectus/vue-tag';
import ControlDescriptorPlugin from '@aspectus/vue-control-descriptor';
import Plugin from '@/main';

import App from './App.vue';

Vue.use(TagPlugin);

Vue.config.productionTip = false;

Vue.use(Plugin);
Vue.use(ControlDescriptorPlugin);
Vue.component('vee-validation-observer', ValidationObserver);
Vue.component('control-custom', {
  props: ['value'],
  render(h) {
    return h('div', {}, [
      h('textarea', { domProps: { value: this.value }, on: { ...this.$listeners, input: e => this.$emit('input', e) } }),
    ]);
  },
});
Vue.component('control-custom-input', {
  props: ['value'],
  render(h) {
    return h('div', {}, [
      h('input', { domProps: { value: this.value }, on: { ...this.$listeners, input: e => this.$emit('input', e) } }),
    ]);
  },
});

extend('required', { ...required, message: 'This is required' });
extend('min', min);

new Vue({
  render: h => h(App),
}).$mount('#app');
