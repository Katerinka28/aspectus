import { makeF } from '@aspectus/vue-utils';

export default makeF(
  (h, context) => h(context.props.descriptorComponent, context.data, context.children),
  { descriptorComponent: {} },
  'vee-control-descriptor-controller-none'
);
