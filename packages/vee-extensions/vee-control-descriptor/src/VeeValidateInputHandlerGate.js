/* eslint-disable prefer-object-spread */

export default {
  render(h) {
    const context = {
      attrs: Object.assign({ value: this.value }, this.$attrs),
      on: Object.assign({}, this.$listeners, {
        input: this.input, blur: this.blur,
      }),
      scopedSlots: this.$scopedSlots,
    };

    return h(this.gatedHandlerComponent, context, null);
  },
  methods: {
    input(e) {
      this.$emit('input', e);
    },
    blur(e) {
      this.$emit('blur', e);
    },
  },
  inheritAttrs: false,
  props: {
    gatedHandlerComponent: {},
    value: {},
  },
  name: 'vee-validate-input-handler-gate',
};
