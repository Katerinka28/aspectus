# Vee optional provider

Vee validate component that renders `ValidationProvider` only when `rules` prop is not empty.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vee-optional-provider.html)
