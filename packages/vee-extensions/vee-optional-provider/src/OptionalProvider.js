import { ValidationProvider } from 'vee-validate';
import { renderSlim, mergeContext, isEmpty, makeF } from '@aspectus/vue-utils';

export default makeF(
  (h, context) => {
    const { rules } = context.props;

    if (isEmpty(rules)) {
      return renderSlim(context.scopedSlots.default(), h);
    }

    return h(
      ValidationProvider,
      mergeContext(context.data, { props: { rules } }),
      context.children
    );
  },
  { rules: {} },
  'vee-optional-provider'
);
