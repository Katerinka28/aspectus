import OptionalProvider from './OptionalProvider';

export function install(Vue, { name = OptionalProvider.name } = {}) {
  Vue.component(name, OptionalProvider);
}

export { OptionalProvider };
export default { install };
