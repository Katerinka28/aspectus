# Vue all selection controller

Component to handle multiple elements selection with `select all` action. Similar to gmail's multiple email selection behavior with ability to select those that are not on this page and exclude any from that selection.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-all-selection-controller.html)
