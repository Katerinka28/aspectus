import { AllSelectionController } from './lib';

function install(Vue, { name = AllSelectionController.name } = {}) {
  Vue.component(name, AllSelectionController);
}

export default { install };
export * from './lib';
