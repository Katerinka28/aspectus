# Vue BEM styled tag

Styled tag generator for vue.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-bem-styled-tag.html)
