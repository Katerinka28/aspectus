import { shallowMount } from '@vue/test-utils';
import { createBlockGenerator, createStateGenerator } from '@aspectus/bem';
import { generator } from '@/main';

export const BEM_CONFIG = { n: 'ns-' };
export const b = createBlockGenerator(BEM_CONFIG);
export const state = createStateGenerator();

describe('Tag generator', () => {
  it('Default tag', () => {
    const Tag = generator({ block: b('tag'), state });
    const wrapper = shallowMount(Tag, {
      attrs: {
        styling: 'default',
        disabled: true,
      },
    });

    expect(wrapper.classes()).toContain('ns-tag');
    expect(wrapper.classes()).toContain('ns-tag--styling_default');
    expect(wrapper.classes()).toContain('is-disabled');
  });

  it('Custom modifiers', () => {
    const Tag = generator({
      state,
      block: b('tag'),
      modifiers: { from: 'to', same: 'same' },
    });
    const wrapper = shallowMount(Tag, {
      attrs: {
        styling: 'default',
        disabled: true,
        from: 'value',
        same: 'value',
      },
    });

    expect(wrapper.classes()).toContain('ns-tag');
    expect(wrapper.classes()).toContain('ns-tag--styling_default');
    expect(wrapper.classes()).toContain('ns-tag--to_value');
    expect(wrapper.classes()).toContain('ns-tag--same_value');
    expect(wrapper.classes()).toContain('is-disabled');
  });

  it('Custom states', () => {
    const Tag = generator({
      state,
      block: b('tag'),
      states: { from: 'to', same: 'same' },
    });
    const wrapper = shallowMount(Tag, {
      attrs: {
        styling: 'default',
        disabled: true,
        from: true,
        same: true,
      },
    });

    expect(wrapper.classes()).toContain('ns-tag');
    expect(wrapper.classes()).toContain('ns-tag--styling_default');
    expect(wrapper.classes()).toContain('is-to');
    expect(wrapper.classes()).toContain('is-same');
    expect(wrapper.classes()).toContain('is-disabled');
  });

  it('Children wrapper', () => {
    const Tag = generator({
      state,
      block: b('tag'),
      children: (h, context, { block }) => [h(
        'div', { class: [block('body')] }, context.children
      )],
    });
    const wrapper = shallowMount(Tag, {
      attrs: {
        disabled: true,
      },
    });

    expect(wrapper.classes()).toContain('ns-tag');
    expect(wrapper.classes()).toContain('is-disabled');
    expect(wrapper.contains('div.ns-tag__body')).toBe(true);
  });

  it('Props transform', () => {
    const Tag = generator({
      state,
      block: b('tag'),
      modifiers: { computed: 'size' },
      transform: props => ({ ...props, computed: props.computed * 3 }),
    });
    const wrapper = shallowMount(Tag, {
      attrs: {
        disabled: true,
        computed: 4,
      },
    });

    expect(wrapper.classes()).toContain('ns-tag');
    expect(wrapper.classes()).toContain('ns-tag--size_12');
    expect(wrapper.classes()).toContain('is-disabled');
  });
});
