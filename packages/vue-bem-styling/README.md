# Vue BEM styling

Set of vue utilities to make blocks styling easier.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-bem-styling.html)
