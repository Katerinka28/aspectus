export const BLOCK_MODIFIERS = {
  theme: 'theme',
  view: 'view',
  styling: 'styling',
  variant: 'variant',
  appearance: 'appearance',
  kind: 'kind',

  align: 'align',
  fit: 'fit',
};

export const BLOCK_STATES = {
  disabled: 'disabled',
  passive: 'passive',
  readonly: 'readonly',
  required: 'required',
  invalid: 'invalid',
  active: 'active',
  opened: 'opened',
  hidden: 'hidden',
};
