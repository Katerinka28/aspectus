/*
  eslint-disable
    max-classes-per-file,
    prefer-spread,
    class-methods-use-this,
    prefer-object-spread
*/

import { createBlockNameGenerator, createStateGenerator } from '@aspectus/bem';

function getContext(vnode) {
  return vnode.fnContext || vnode.context;
}

function getBlockName(options, key) {
  return options[key] || options.blockName || options.block || options.name;
}

export class Directive {
  constructor(config) {
    this.config = config;
    this.generator = this.getGenerator(config);

    this.bind = this.bind.bind(this);
    this.update = this.update.bind(this);
    this.unbind = this.unbind.bind(this);
  }

  addClasses(el, classes) {
    el.classList.add.apply(el.classList, classes);
  }

  removeClasses(el, classes) {
    el.classList.remove.apply(el.classList, classes);
  }

  bind() { }

  update() { }

  unbind() { }
}

export class BlockDirective extends Directive {
  getGenerator(config) {
    return createBlockNameGenerator(config);
  }

  getBlockName(binding, vnode) {
    return binding.arg || getBlockName(getContext(vnode).$options, this.config.blockNameKey);
  }

  getElementName(binding) {
    /* eslint-disable no-restricted-syntax, prefer-destructuring, no-prototype-builtins */

    const modifiers = binding.modifiers;

    if (modifiers) {
      for (const key in modifiers) {
        if (modifiers.hasOwnProperty(key)) {
          return key;
        }
      }
    }

    return undefined;
  }

  bind(el, binding, vnode) {
    const blockName = this.getBlockName(binding, vnode);

    if (!blockName) return;

    this.addClasses(el, this.generator(
      blockName, this.getElementName(binding), binding.value
    ));
  }

  update(el, binding, vnode) {
    const blockName = this.getBlockName(binding, vnode);

    if (!blockName) return;

    this.removeClasses(
      el, this.generator(blockName, this.getElementName(binding), binding.oldValue)
    );
    this.addClasses(
      el, this.generator(blockName, this.getElementName(binding), binding.value)
    );
  }
}

export class BlockDirectiveUnbindable extends BlockDirective {
  unbind(el, binding, vnode) {
    const blockName = this.getBlockName(binding, vnode);

    if (!blockName) return;

    this.removeClasses(el, this.generator(
      blockName, this.getElementName(binding), binding.value
    ));
  }
}

export class StateDirective extends Directive {
  getGenerator(config) {
    return createStateGenerator(config);
  }

  bind(el, binding) {
    this.addClasses(el, this.generator(Object.assign({}, binding.modifiers, binding.value)));
  }

  update(el, binding) {
    this.removeClasses(el, this.generator(binding.oldValue));
    this.addClasses(el, this.generator(binding.value));
  }
}

export class StateDirectiveUnbindable extends StateDirective {
  unbind(el, binding) {
    this.removeClasses(el, this.generator(Object.assign({}, binding.modifiers, binding.value)));
  }
}
