# Vue control descriptor

Control descriptor component. Special wrapper for control element that adds additional description.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-control-descriptor.html)
