const path = require('path');

module.exports = {
  css: {
    loaderOptions: {
      postcss: {
        config: {
          path: path.join(__dirname, 'postcss.config.js'),
        },
      },
    },
  },
};
