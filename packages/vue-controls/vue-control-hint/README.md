# Vue control hint

Simple functional component for `control-hint` element.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-control-hint.html)
