import { renderSlim } from '@aspectus/vue-utils';

export default {
  name: 'vue-control-interaction-state-controller',
  inheritAttrs: false,
  props: ['value'],
  data() {
    return {
      interacted: false,
      interacting: false,
      changed: false,
      changing: false,
    };
  },
  render(h) {
    return renderSlim(this.$scopedSlots.default({
      interacted: this.interacted,
      interacting: this.interacting,
      changed: this.changed,
      changing: this.changing,
      value: this.value,

      input: this.input,
      focus: this.focus,
      blur: this.blur,

      handlers: {
        input: this.input,
        focus: this.focus,
        blur: this.blur,
      },
    }), h);
  },
  methods: {
    interact(value) {
      if (!this.interacted) {
        this.interacted = true;
      }

      if (this.interacting !== value) {
        this.interacting = value;
      }
    },
    input(event) {
      if (!this.changed) {
        this.changed = true;
      }

      if (this.interacting && !this.changing) {
        this.changing = true;
      }

      this.interact(this.interacting);
      this.$emit('input', event);
    },
    focus(event) {
      this.interact(true);

      this.$emit('focus', event);
    },
    blur(event) {
      this.interact(false);
      this.changing = false;

      this.$emit('blur', event);
    },
  },
};
