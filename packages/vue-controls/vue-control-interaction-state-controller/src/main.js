import InteractionStateController from './InteractionStateController';

export function install(Vue, { name = InteractionStateController.name } = {}) {
  Vue.component(name, InteractionStateController);
}

export { InteractionStateController };
export default { install };
