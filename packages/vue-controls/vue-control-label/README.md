# Vue control label

Simple functional component for `control-label` element.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-control-label.html)
