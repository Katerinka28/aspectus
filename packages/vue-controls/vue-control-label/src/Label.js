import { generator } from '@aspectus/vue-bem-styled-tag';
import { Tag } from '@aspectus/vue-tag';
import { createBlockGenerator, createStateGenerator } from '@aspectus/bem';

const name = 'control-label';
const b = createBlockGenerator();
const s = createStateGenerator();

export default Object.assign(
  generator({ block: b(name), state: s, tag: Tag }), { name }
);
