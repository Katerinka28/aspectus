import Label from './Label';

export function install(Vue, { name = Label.name } = {}) {
  Vue.component(name, Label);
}

export { Label };
export default { install };
