import { tag } from '../utils';
import { SIZE } from '../const';

const children = (h, context, { block }) => {
  if (context.children) {
    return context.scopedSlots.default({ block, ...context.props });
  }

  const result = [];
  const { src, label } = context.props;

  if (src) {
    result.push(h(
      'img', { class: block('image'), attrs: { src, alt: label } }
    ));
  } else if (label && label.charAt) {
    result.push(h(
      'div', { class: block('label') }, [label.charAt(0)]
    ));
  }

  return result;
};

const [Avatar] = tag('avatar', {
  modifiers: SIZE,
  props: {
    src: String,
    label: String,
  },
  children,
});

export default Avatar;
