import { withDefaultProps } from '@aspectus/vue-utils';
import { Tag } from '@aspectus/vue-tag';
import { t, u, ud } from '../bem';
import { tag, el, body, mapSame } from '../utils';
import { SIZE, SPACE, IN_OF } from '../const';

const dtag = tg => withDefaultProps({ tag: tg }, Tag);

export const [Caption] = tag('caption', { modifiers: SIZE });
export const [Inliner] = tag('inliner', { modifiers: SIZE, children: body });
export const [Link] = tag('link');

export const [AspectRatio, aspectRatioBlock] = tag('aspect-ratio', { modifiers: mapSame('ratio') });
export const AspectRatioBody = el('aspect-ratio-body', aspectRatioBlock, 'body');

export const [Panel, panelBlock] = tag('panel', { modifiers: SPACE });
export const PanelElement = el('panel-element', panelBlock, { modifiers: IN_OF }, 'element');

export const [Section, sectionBlock] = tag('section', { modifiers: SPACE });
export const SectionElement = el('section-element', sectionBlock, { modifiers: IN_OF }, 'element');

export const [Table, tableBlock] = tag('table');
export const TableCaption = el('table-caption', tableBlock, { tag: dtag('caption') }, 'caption');
export const TableRow = el('table-row', tableBlock, { tag: dtag('tr'), modifiers: mapSame('relief') }, 'row');
export const TableCell = el('table-cell', tableBlock, { tag: dtag('td'), modifiers: mapSame('head') }, 'cell');

// Typography
export const [Content] = tag('content', {}, t);
export const [Words] = tag('words', { modifiers: mapSame('fill') }, t);

// Utils
export const [Display, displayBlock] = tag('display', { modifiers: mapSame('hidden', 'block') }, ud);
export const [SpreadedInteractor, spreadedInteractorBlock] = tag('spreaded-interactor', {}, u);
