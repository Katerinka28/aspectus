import partial from 'ramda/src/partial';
import { generator } from '@aspectus/vue-bem-styled-tag';
import { mapSame } from '@aspectus/vue-bem-styling';
import { Tag } from '@aspectus/vue-tag';
import { b } from './bem';

export const tag = (name, options = {}, bg = b) => {
  const block = bg(name);

  return [
    Object.assign(generator({ tag: Tag, block, ...options }), { name }),
    block,
  ];
};

export const el = (name, bl, options = {}, blockName = name) => Object.assign(
  generator({ tag: Tag, block: partial(bl, [blockName]), ...options }),
  { name }
);

export const inner = name => (h, context, { block }) => [h(
  'div', { class: block(name) }, context.children
)];

export const body = inner('body');

export { mapSame };
