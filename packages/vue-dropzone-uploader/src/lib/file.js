/*
  eslint-disable
    prefer-object-spread,
    import/prefer-default-export,
    max-classes-per-file,
    no-return-assign
*/

export class ProgressDescriptor {
  constructor(loaded, total) {
    this.loaded = loaded || 0;
    this.total = total || 0.01;
    this.relative = Math.min((this.loaded / this.total) || 0, 1);
  }
}

export class FileObject {
  constructor(file) {
    this.file = file;
    this.name = this.file.name;
    this.size = this.file.size;
    this.type = this.file.type;
    this.url = null;

    this.updateUrl(this.file);
  }

  updateUrl(file = this.file) {
    const reader = new FileReader();
    reader.onload = e => this.url = e.target.result;
    reader.readAsDataURL(file);
  }
}

export class FileDescriptor {
  constructor(file, id, status) {
    this.id = id;
    this.file = file;
    this.result = null;
    this.status = status;
    this.progress = new ProgressDescriptor(0, 1);
  }
}
