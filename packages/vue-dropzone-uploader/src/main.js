import { Uploader } from './lib';

function install(Vue, { name = Uploader.name } = {}) {
  Vue.component(name, Uploader);
}

export default { install };
export * from './lib';
