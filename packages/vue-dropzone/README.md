# Vue Dropzone component

Simple dropzone component for vue. Based on `@aspectus/dropzone` implementation.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-dropzone.html)
