import { Dropzone } from './lib';

function install(Vue, { name = Dropzone.name } = {}) {
  Vue.component(name, Dropzone);
}

export default { install };
export * from './lib';
