/* eslint-disable class-methods-use-this */

import { EqObserver } from '@aspectus/eq';

export const EQ_EVENT_UPDATE = 'eq:update';
export const EQ_EVENT_UNBIND = 'eq:unbind';

export class Directive {
  constructor({ breakpoints = [] }) {
    this.breakpoints = breakpoints;

    this.bind = this.bind.bind(this);
    this.update = this.update.bind(this);
    this.unbind = this.unbind.bind(this);
  }

  bind(el, { value }) {
    const breakpoints = Array.isArray(value) ? value : this.breakpoints || [];
    const observer = new EqObserver({ breakpoints }).bind(el);

    const updator = e => {
      observer.update({ breakpoints: e.detail.breakpoints || [] });
    };
    const unbinder = () => {
      observer.unbind(el);
      observer.clear();

      el.removeEventListener(EQ_EVENT_UPDATE, updator);
      el.removeEventListener(EQ_EVENT_UNBIND, unbinder);
    };

    el.addEventListener(EQ_EVENT_UPDATE, updator);
    el.addEventListener(EQ_EVENT_UNBIND, unbinder);
  }

  update(el, { value }) {
    const breakpoints = Array.isArray(value) && value || this.breakpoints || [];

    el.dispatchEvent(new CustomEvent(EQ_EVENT_UPDATE, { detail: { breakpoints } }));
  }

  unbind(el) {
    el.dispatchEvent(new CustomEvent(EQ_EVENT_UNBIND));
  }
}
