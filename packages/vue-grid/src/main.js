import { Cell, Row, Container, cols, b } from './lib';

export function install(Vue, {
  containerName = Container.name,
  rowName = Row.name,
  cellName = Cell.name,
} = {}) {
  Vue.component(containerName, Container);
  Vue.component(rowName, Row);
  Vue.component(cellName, Cell);
}

export { Cell, Row, Container, cols, b };
export default {
  Cell, Row, Container, cols, b, install,
};
