import { createLocalVue } from '@vue/test-utils';
import Plugin, { Row, Cell, Container } from '@/main';

describe('Plugin', () => {
  it('Installs silently', () => {
    const localVue = createLocalVue();

    expect(() => localVue.use(Plugin)).not.toThrow();
  });

  it('Installs components', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin);

    expect(localVue.options.components[Row.name]).toBeDefined();
    expect(localVue.options.components[Cell.name]).toBeDefined();
    expect(localVue.options.components[Container.name]).toBeDefined();
  });

  it('Installs custom named components', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin, {
      rowName: 'v-row',
      cellName: 'v-cell',
      containerName: 'v-container',
    });

    expect(localVue.options.components[Row.name]).not.toBeDefined();
    expect(localVue.options.components[Cell.name]).not.toBeDefined();
    expect(localVue.options.components[Container.name]).not.toBeDefined();

    expect(localVue.options.components['v-row']).toBeDefined();
    expect(localVue.options.components['v-cell']).toBeDefined();
    expect(localVue.options.components['v-container']).toBeDefined();
  });
});
