import { Row } from '@/main';
import { sm } from './utils';

describe('Row', () => {
  it('Mounts', () => {
    expect(sm(Row).contains('div')).toBe(true);
    expect(sm(Row, { propsData: { tag: 'span' } }).contains('span')).toBe(true);
  });

  it('Classes check', () => {
    const rowBase = sm(Row);

    expect(rowBase.classes()).toContain('g-row');
    expect(rowBase.classes()).toHaveLength(1);

    const rowCustomClasses = sm(Row, {
      context: {
        class: ['one', { two: true, three: false }],
      },
    });

    expect(rowCustomClasses.classes()).toContain('g-row');
    expect(rowCustomClasses.classes()).toContain('one');
    expect(rowCustomClasses.classes()).toContain('two');
    expect(rowCustomClasses.classes()).not.toContain('three');
    expect(rowCustomClasses.classes()).toHaveLength(3);
  });

  it('Modifiers check', () => {
    const rowBase = sm(Row, {
      propsData: {
        align: 'center',
      },
    });

    expect(rowBase.classes()).toContain('g-row');
    expect(rowBase.classes()).toContain('g-row--align_center');
    expect(rowBase.classes()).toHaveLength(2);

    const rowCustomClasses = sm(Row, {
      context: {
        class: ['one', { two: true, three: false }],
      },
      propsData: {
        justify: ['center', 'left-lg'],
        align: ['center', 'left-md'],
        space: 10,
        appearance: 'nowrap',
        content: 'center',
      },
    });

    expect(rowCustomClasses.classes()).toContain('g-row');
    expect(rowCustomClasses.classes()).toContain('g-row--justify_center');
    expect(rowCustomClasses.classes()).toContain('g-row--justify_left-lg');
    expect(rowCustomClasses.classes()).toContain('g-row--align_center');
    expect(rowCustomClasses.classes()).toContain('g-row--align_left-md');
    expect(rowCustomClasses.classes()).toContain('g-row--space_10');
    expect(rowCustomClasses.classes()).toContain('g-row--appearance_nowrap');
    expect(rowCustomClasses.classes()).toContain('g-row--content_center');
    expect(rowCustomClasses.classes()).toContain('one');
    expect(rowCustomClasses.classes()).toContain('two');
    expect(rowCustomClasses.classes()).not.toContain('three');
    expect(rowCustomClasses.classes()).toHaveLength(10);
  });
});
