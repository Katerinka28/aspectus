import { cols } from '@/main';

describe('Cols', () => {
  it('Simple string generation', () => {
    const simple = cols('12');

    expect(simple).toContain('g-cols');
    expect(simple).toContain('g-cols--12');
    expect(simple).toHaveLength(2);

    const simple2 = cols('12', '5');

    expect(simple2).toContain('g-cols');
    expect(simple2).toContain('g-cols--12');
    expect(simple2).toContain('g-cols--5');
    expect(simple2).toHaveLength(3);
  });

  it('Array flatten', () => {
    const simple = cols(['12']);

    expect(simple).toContain('g-cols');
    expect(simple).toContain('g-cols--12');
    expect(simple).toHaveLength(2);

    const simple2 = cols(['12', '5']);

    expect(simple2).toContain('g-cols');
    expect(simple2).toContain('g-cols--12');
    expect(simple2).toContain('g-cols--5');
    expect(simple2).toHaveLength(3);
  });

  it('Parse string', () => {
    const simple = cols('12 auto-md');

    expect(simple).toContain('g-cols');
    expect(simple).toContain('g-cols--12');
    expect(simple).toContain('g-cols--auto-md');
    expect(simple).toHaveLength(3);

    const simple2 = cols(['12', '5 10-lg'], '8 4-md 2-lg');

    expect(simple2).toContain('g-cols');
    expect(simple2).toContain('g-cols--12');
    expect(simple2).toContain('g-cols--5');
    expect(simple2).toContain('g-cols--10-lg');
    expect(simple2).toContain('g-cols--8');
    expect(simple2).toContain('g-cols--4-md');
    expect(simple2).toContain('g-cols--2-lg');
    expect(simple2).toHaveLength(7);
  });
});
