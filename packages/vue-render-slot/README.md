# Vue `render-slot` component

Functional component for rendering any scoped slot.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-render-slot.html)
