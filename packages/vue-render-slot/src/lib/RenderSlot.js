import { renderSlim, makeF } from '@aspectus/vue-utils';
import { Tag } from '@aspectus/vue-tag';

export default makeF(
  (h, context) => {
    const { slot, props } = context.props;

    return renderSlim(slot(props, context), h, Tag, context.data);
  },
  ['slot', 'props'],
  'render-slot'
);
