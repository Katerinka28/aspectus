export default function routeInstaller(View, Installer, Vue) {
  return () => Promise.all([View(), Installer()])
    .then(result => {
      const ViewComponent = result[0];
      const installer = result[1];
      Vue.use(installer.default || installer.install || installer);

      return ViewComponent;
    });
}
