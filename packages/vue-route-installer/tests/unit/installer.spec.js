import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';

import installer from '@/main';

describe('Installer', () => {
  it('Installs plugin', async () => {
    const localVue = createLocalVue();
    const Installer = () => import('./fixtures/Installer');
    const View = () => import('./fixtures/View');
    const V = installer(View, Installer, localVue);
    let promise = Promise.resolve(null);
    localVue.use(VueRouter);

    const router = new VueRouter({
      routes: [
        {
          path: '/',
          component: () => {
            promise = V();
            return promise;
          },
        },
      ],
    });
    expect(localVue.options.components['installed-component']).not.toBeDefined();

    const wrapper = mount({
      template: '<div><router-view /></div>',
    }, {
      localVue,
      router,
    });

    router.push('/');

    await wrapper.vm.$nextTick();
    await promise;

    expect(localVue.options.components['installed-component']).toBeDefined();
    expect(wrapper.contains('h1')).toBe(true);
  });
});
