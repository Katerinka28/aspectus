import omit from 'ramda/src/omit';
import pick from 'ramda/src/pick';
import { renderSlim } from '@aspectus/vue-utils';
import { Tag } from '@aspectus/vue-tag';

const same = x => x;
const pathOmitter = omit(['path']);
const routerPicker = pick(['query', 'path', 'params', 'name']);
const routerOmitter = value => ('name' in value ? pathOmitter(value) : value);
const INTERNAL_KEY = '__internal__';

export default {
  name: 'router-parameters-controller',

  props: {
    initial: {
      type: Object,
      default: () => ({}),
    },
    from: {
      type: Function,
      default: same,
    },
    to: {
      type: Function,
      default: same,
    },
  },

  data() {
    return {
      parameters: this.initial,
      hadChanged: false,
    };
  },

  watch: {
    $route: {
      immediate: true,
      handler(value, oldValue) {
        this.parameters = this.from(Object.assign( // eslint-disable-line prefer-object-spread
          {}, this.initial, value.params, value.query
        ));

        const event = { parameters: this.parameters };

        this.$emit('change', event);

        if (value.params[INTERNAL_KEY]) {
          this.$emit('change:internal', event);

          return;
        }

        if (oldValue) {
          this.$emit('change:external', event);
        }

        if (!oldValue || !this.hadChanged) {
          this.$emit('change:initial', event);
          this.hadChanged = true;
        }
      },
    },
  },

  methods: {
    updateUrl(parameters) {
      const parameterKeys = Object.keys(this.$route.params);
      const paramsPicker = pick(parameterKeys);
      const queryGetter = this.$route.name ? omit(parameterKeys) : same;
      const query = this.to(parameters);

      this.$router
        .push(routerOmitter(routerPicker(Object.assign( // eslint-disable-line prefer-object-spread
          {},
          this.$route,
          {
            query: queryGetter(query),
            params: Object.assign( // eslint-disable-line prefer-object-spread
              {},
              this.$route.params,
              paramsPicker(query),
              {
                [INTERNAL_KEY]: true,
              }
            ),
          }
        ))))
        .catch(() => { }); // For the same URL vue-router throws here.
    },

    changeParameters(value) {
      this.updateUrl(value);
    },
  },

  render(h) {
    return renderSlim(this.$scopedSlots.default({
      parameters: this.parameters,
      changeParameters: this.changeParameters,
      updateUrl: this.updateUrl,
    }), h, Tag);
  },
};
