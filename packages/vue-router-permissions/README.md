# Vue router permissions

Mechanics for applying permissions check for vue router views and links.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-router-permissions.html)
