# Vue selection controller

Vue implementation for `@aspectus/selection-controller` mechanism.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-selection-controller.html)
