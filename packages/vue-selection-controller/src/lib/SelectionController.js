import { updateValue, prepareData, resultValue } from '@aspectus/selection-controller';
import { renderSlim } from '@aspectus/vue-utils';
import { Tag } from '@aspectus/vue-tag';

export default {
  name: 'vue-selection-controller',

  props: {
    multiple: {
      type: Boolean,
      default: false,
    },
    value: {},
    options: {},
    keyGetter: Function,
    updator: {
      type: Function,
      default: updateValue,
    },
    states: Object,
  },

  computed: {
    prepared() {
      return prepareData(this.options, this.value, this.keyGetter, this.states);
    },
  },

  methods: {
    change(change) {
      const value = this.updator(
        change, this.prepared.value, this.keyGetter, this.multiple
      );

      this.$emit('input', resultValue(value, this.multiple));
    },
  },

  render(h) {
    const { change, multiple, keyGetter } = this;
    const { value, options } = this.prepared;

    return renderSlim(this.$scopedSlots.default({
      value, options, multiple, change, keyGetter,
    }), h, Tag);
  },
};
