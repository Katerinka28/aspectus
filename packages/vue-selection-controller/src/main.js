import { SelectionController } from './lib';

function install(Vue, { name = SelectionController.name } = {}) {
  Vue.component(name, SelectionController);
}

export default { install };
export * from './lib';
