# Vue `tag` component

Component for custom tag usage inside components. It's used to replace vue built in `:is` mechanism because it's working in a slightly different way(do not know whether it's a bug or a feature).

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-tag.html)
