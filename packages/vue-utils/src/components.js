/* eslint-disable import/prefer-default-export, no-underscore-dangle */

export const getComponentOptions = Component => (
  typeof Component === 'function' ? Component.options : Component
);

export const makeF = (render, props, name, rest = {}) => Object.assign(rest, {
  name,
  functional: true,
  props,
  render,
});
