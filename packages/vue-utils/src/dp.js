/* eslint-disable import/prefer-default-export, no-underscore-dangle */

export function resolveInjection(key, vm) {
  let source = vm;

  while (source) {
    if (source._provided && Object.prototype.hasOwnProperty.call(source._provided, key)) {
      return source._provided[key];
    }
    source = source.$parent;
  }

  return null;
}
