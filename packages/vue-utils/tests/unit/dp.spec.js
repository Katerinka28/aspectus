import { shallowMount } from '@vue/test-utils';
import { resolveInjection } from '@/main';

describe('Injection resolver', () => {
  const Provider = {
    provide: { className: 'value' },
    render(h) { return h('div', null, this.$scopedSlots.default()); },
  };

  const Consumer = {
    render(h) {
      return h('div', { class: resolveInjection('className', this) });
    },
  };

  const FunctionalConsumer = {
    functional: true,
    render(h, context) {
      return h('div', { class: resolveInjection('className', context.parent) });
    },
  };

  it('Inside children', () => {
    const wrapper = shallowMount(
      { template: '<provider><consumer /></provider>' }, {
        stubs: {
          Provider,
          Consumer,
        },
      }
    );

    const consumer = wrapper.find(Consumer);
    expect(consumer.classes()).toContain('value');
  });

  it('Deeply nested', () => {
    const ContainerFirst = {
      render(h) { return h('div', null, [h('container-second')]); },
    };
    const ContainerSecond = { render(h) { return h('container-final'); } };
    const ContainerFinal = { render(h) { return h('consumer'); } };

    const wrapper = shallowMount(
      { template: '<provider><container-first /></provider>' }, {
        stubs: {
          Provider,
          Consumer,
          ContainerFirst,
          ContainerSecond,
          ContainerFinal,
        },
      }
    );

    const consumer = wrapper.find(Consumer);
    expect(consumer.classes()).toContain('value');
  });

  it('Functional consumer', () => {
    // It can only work if the functional component is inside container.
    // It's Vue's implementation issue - not mine.
    const Container = { render(h) { return h('f-consumer'); } };

    const wrapper = shallowMount(
      { template: '<provider><container /></provider>' }, {
        stubs: {
          Provider,
          Container,
          'f-consumer': FunctionalConsumer,
        },
      }
    );

    const consumer = wrapper.find(FunctionalConsumer);
    expect(consumer.classes()).toContain('value');
  });
});
